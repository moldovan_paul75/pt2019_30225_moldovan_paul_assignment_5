package pt2019.homework5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {
	
	private List<MonitoredData> data = new ArrayList<MonitoredData>();
	
	public ReadFile() {
		String fileName = "C:\\Users\\moldo\\Desktop\\F\\An2\\Sem2\\TP\\PT2019\\pt2019_30225_moldovan_paul_assignment_5\\Activities.txt";
		List<String> list = new ArrayList<>();

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			list = stream.collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}

		//list.forEach(System.out::println);
		list.forEach( n->{
			//System.out.println(n.substring(0, 19));
			//System.out.println(n.substring(21, 40));
			//System.out.println(n.substring(42, n.length()-1);
			MonitoredData data1 = new MonitoredData(n.substring(0, 19), n.substring(21, 40), n.substring(42, n.length()-1));
			data.add(data1);
		});
		this.sortData();
	}
	
	private void sortData() {
		data.sort((MonitoredData m1, MonitoredData m2) ->{
			if(m1.getStartTimeDate().after(m2.getStartTimeDate())) {
				return 1;
			}
			return -1;
		}
		);
	}
	
	public List<MonitoredData> getList(){
		return this.data;
	}
}
