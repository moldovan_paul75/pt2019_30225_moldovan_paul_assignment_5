package pt2019.homework5;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Tasks {
	
	private ReadFile file = new ReadFile();
	private List<MonitoredData> data = file.getList();
	
	public int countDays() {
		return (int) data.stream().map(i -> i.getEndTimeDate().getDate()).distinct().count();
	}
	
	public Map<Object, Long> countActivities() {
		return data.stream().collect(
					Collectors.groupingBy(i -> i.getActivityLabel(), Collectors.counting()
					)
				);
	}
	
	public void countActivitiesByDay() {
		 System.out.println(data.stream().collect(
								Collectors.groupingBy(i -> i.getStartTimeDate().getDate(),
											Collectors.groupingBy(j -> j.getActivityLabel(), Collectors.counting())
								)
						 )+"\n"
				 );
	}
	
	public void activityDuration() {
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		data.forEach(i ->{
			System.out.println(i.getActivityLabel() + ": " + 
								formatter.format(
										new Date(
												i.getEndTimeDate().getTime() - i.getStartTimeDate().getTime()
										)
					)
			);
		});
		System.out.println("\n");
	}
	
	
	public void activitiesDuration() {		
		System.out.println(
				data.stream().collect(
					Collectors.groupingBy(i -> i.getActivityLabel(),
							Collectors.summingLong(j -> (j.getEndTimeDate().getTime() - j.getStartTimeDate().getTime()))
							)
					) + "\n"
			);
	}
	
	public void filterActivities() {	
		Map<Object, Long> map = this.countActivities();
		
		Map<Object, Long> map2 =
				data.stream()
							.filter(i -> (i.getEndTimeDate().getTime() - i.getStartTimeDate().getTime()) <= 300000)
							.collect(
									Collectors.groupingBy(i -> i.getActivityLabel(), Collectors.counting()
											)
									);
		
		map2.entrySet().stream().forEach(i->{
			map.entrySet().stream().forEach(j->{
				if(i.getKey().equals(j.getKey())) {
					if(i.getValue()/j.getValue() > 90/100) {
						System.out.println(i.getKey());
					}
				}
				});
		});
	}
}
