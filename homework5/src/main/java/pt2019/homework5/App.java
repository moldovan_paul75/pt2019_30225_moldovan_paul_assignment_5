package pt2019.homework5;

import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
    	Tasks t = new Tasks();
    	System.out.println("Task1\n" + t.countDays()+"\n");
    	System.out.println("Task2\n" +t.countActivities()+"\n");
    	System.out.println("Task3");
    	t.countActivitiesByDay();
    	System.out.println("Task4");
    	t.activityDuration();
    	System.out.println("Task5");
    	t.activitiesDuration();
    	System.out.println("Task6");
    	t.filterActivities();
    }
}
