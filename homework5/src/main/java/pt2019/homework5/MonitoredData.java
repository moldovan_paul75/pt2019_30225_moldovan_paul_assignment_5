package pt2019.homework5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MonitoredData {
	
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	private SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//private DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public MonitoredData(String start, String end, String activity) {
		this.activityLabel = activity;
		
		try {
			this.startTime = format1.parse(start);
			this.endTime = format1.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getStartTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime);
	}
	
	public String getEndTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime);
	}
	
	public String getActivityLabel() {
		return this.activityLabel;
	}
	
	public Date getStartTimeDate() {
		return this.startTime;
	}
	
	public Date getEndTimeDate() {
		return this.endTime;
	}
	
	@Override
	public String toString() {
		return 	  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTime) + "      " 
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTime) + "      " 
				+ activityLabel;
	}
		
		
}
